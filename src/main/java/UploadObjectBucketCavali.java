
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.canvia.constancias.bean.PreSignedUrlResponseIn;
import com.canvia.constancias.service.impl.SendFileServiceImpl;

public class UploadObjectBucketCavali {

	public static void main(String[] args) throws Exception {

		SendFileServiceImpl sendFileServiceImpl = new SendFileServiceImpl();

		// Se arma request
		PreSignedUrlResponseIn preSignedUrlResponseIn = new PreSignedUrlResponseIn();
		preSignedUrlResponseIn.setFilename("sample.pdf");
		preSignedUrlResponseIn.setProcessId("123");

		// Se obtiene url pre firmada para cargar archivo en bucket
		String presignedUrl = sendFileServiceImpl.getPreSignedUrl(preSignedUrlResponseIn);
		URL url = new URL(presignedUrl);
		// Se carga archivo al bucket
//			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//			connection.setDoOutput(true);
//			connection.setRequestMethod("PUT");
//			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
//			out.write("This text uploaded as an object via presigned URL.");
//			out.close();

		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connection.setRequestMethod("PUT");

		InputStream in = new FileInputStream("src/file/sample.pdf");
		OutputStream out = connection.getOutputStream();
		copy(in, connection.getOutputStream());

		out.flush();
		out.close();

		// Se obtiene codigo de respuesta del Http Status Code
		connection.getResponseCode();
		System.out.println("HTTP response code: " + connection.getResponseCode());

	}

	protected static long copy(InputStream input, OutputStream output) throws IOException {
		byte[] buffer = new byte[12288]; // 12K
		long count = 0L;
		int n = 0;
		while (-1 != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
			count += n;
		}
		return count;
	}
}