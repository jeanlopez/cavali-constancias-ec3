import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.S3Object;

public class GeneratePresignedUrlAndUploadObject {

    public static void main(String[] args) throws IOException {
        //Se indica la region a la cual esta apuntando, esto es para crear la url prefirmada
    	Regions clientRegion = Regions.US_EAST_1;
    	//se indica el nombre del bucket, esto es para crear la url prefirmada
        String bucketName = "cavali-constancias";
        //se coloca el key, esto es para crear la url prefirmada
        String objectKey = "constancias/pdf1.pdf";
        
        try {
            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withCredentials(new ProfileCredentialsProvider())
                    .withRegion(clientRegion)
                    .build();

            //Se configura el tiempo de expiracion de la URL prefirmada
            //esta configurado para que expire luego de una hora
            java.util.Date expiration = new java.util.Date();
            long expTimeMillis = expiration.getTime();
            expTimeMillis += 1000 * 60 * 60;
            expiration.setTime(expTimeMillis);

            // Se genera la url prefirmada
            System.out.println("Generating pre-signed URL.");

            GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, objectKey)
                    .withMethod(HttpMethod.PUT)
                    .withExpiration(expiration);
            URL url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);

            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestMethod("PUT");

            InputStream in = new FileInputStream("src/file/sample.pdf");
            OutputStream out = con.getOutputStream();
            copy(in, con.getOutputStream());
            
            out.flush();
            out.close();
            
            // Check the HTTP response code. To complete the upload and make the object available, 
            // you must interact with the connection object in some way.
//            connection.getResponseCode();
            con.getResponseCode();
//            System.out.println("HTTP response code: " + connection.getResponseCode());
        	System.out.println("HTTP response code: " + con.getResponseCode());

            // Check to make sure that the object was uploaded successfully.
            S3Object object = s3Client.getObject(bucketName, objectKey);
            System.out.println("Object " + object.getKey() + " created in bucket " + object.getBucketName());
        } catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process 
            // it, so it returned an error response.
            e.printStackTrace();
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client  
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
        }
    }
    
	protected static long copy(InputStream input, OutputStream output) throws IOException {
		byte[] buffer = new byte[12288]; // 12K
		long count = 0L;
		int n = 0;
		while (-1 != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
			count += n;
		}
		return count;
	}
}
