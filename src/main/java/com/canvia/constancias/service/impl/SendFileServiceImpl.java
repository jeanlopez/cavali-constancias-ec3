package com.canvia.constancias.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.canvia.constancias.bean.PreSignedUrlResponseIn;
import com.canvia.constancias.bean.PreSignedUrlResponseOut;
import com.canvia.constancias.bean.TokenResponseOut;
import com.canvia.constancias.service.ISendFileService;
import com.canvia.constancias.util.Constants;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class SendFileServiceImpl implements ISendFileService {

	private Logger logger = LoggerFactory.getLogger(ISendFileService.class);

//	@Value("${credentials.firma.token.url}")
//	private String firmaServiceTokenUrl;
//
//	@Value("${credentials.firma.url}")
//	private String firmaServiceUrl;
//
//	@Value("${credentials.firma.client_id}")
//	private String firmaServiceClientId;
//
//	@Value("${credentials.firma.client_secret}")
//	private String firmaServiceClientSecret;
//
//	@Value("${credentials.firma.grant_type}")
//	private String firmaServiceGrantType;
//
//	@Value("${credentials.firma.x-api-key}")
//	private String firmaServiceXApiKey;

	
	//Se obtiene url pre firmada
	public String getPreSignedUrl(PreSignedUrlResponseIn preSignedUrlResponseIn) throws Exception {
		String firmaServiceUrl = "https://api.dev.cavali.com.pe/cd/v1/document";
		String firmaServiceXApiKey = "QCF5VXIoSN2ArS2ufJmyk3X79KrfGwmm7OxYc51q";

		PreSignedUrlResponseOut preSignedUrlResponseOut = null;
		String preSignedUrl = null;

		try {
			HttpHeaders headers = new HttpHeaders();
			RestTemplate restTemplate = new RestTemplate();

			TokenResponseOut token = getToken();

			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("x-api-key", firmaServiceXApiKey);
			headers.add("Authorization", "Bearer " + token.getAccessToken());

			ObjectMapper Obj = new ObjectMapper();

			String jsonStr = Obj.writeValueAsString(preSignedUrlResponseIn);
			HttpEntity<String> request = new HttpEntity<>(jsonStr, headers);

			ResponseEntity<PreSignedUrlResponseOut> response = restTemplate.exchange(firmaServiceUrl, HttpMethod.POST,
					request, PreSignedUrlResponseOut.class);

			preSignedUrlResponseOut = response.getBody();

			if (Constants.WS_SIGNER_STATUS_OK == response.getStatusCodeValue()) {
				preSignedUrl = preSignedUrlResponseOut.getPreSignedURL();
			} else {
				logger.info("Body Signer no válido, " + preSignedUrlResponseOut.getBody());
				throw new Exception("Ocurrió un problema al consultar Signature, intente más tarde.");
			}

		} catch (Exception ex) {
			logger.error("Error en getPreSignedUrl ", ex);
			throw new Exception("Ocurrio un problema al consultar Servicio PreSignedUrl", ex);
		}
		return preSignedUrl;
	}

	// Obtener Token
	private TokenResponseOut getToken() throws Exception {

		String firmaServiceTokenUrl = "https://cavali-cd-dev.auth.us-east-1.amazoncognito.com/oauth2/token";
		String firmaServiceClientId = "4hkpd58qcove7gg8drt473lte3";
		String firmaServiceClientSecret = "14hs1a8ou3o03giid1197e13jqvbng9al72a17keo6roehlb9ktm";
		String firmaServiceGrantType = "client_credentials";

		TokenResponseOut resp = new TokenResponseOut();
		ResponseEntity<TokenResponseOut> TokenResponseHttp = null;

		try {
			String url = firmaServiceTokenUrl;
			HttpHeaders headers = new HttpHeaders();
			RestTemplate restTemplate = new RestTemplate();
			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			map.add(Constants.WS_SIGNER_PARAM_CLIENT_ID, firmaServiceClientId);
			map.add(Constants.WS_SIGNER_PARAM_CLIENT_SECRET, firmaServiceClientSecret);
			map.add(Constants.WS_SIGNER_PARAM_CLIENT_GRANT_TYPE, firmaServiceGrantType);

			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
			TokenResponseHttp = restTemplate.exchange(url, HttpMethod.POST, request, TokenResponseOut.class);

			resp = TokenResponseHttp.getBody();

		} catch (Exception ex) {
			logger.error("Error en getToken ", ex);
			throw new Exception("Ocurrio un problema al obtener Token", ex);
		}

		return resp;
	}
}