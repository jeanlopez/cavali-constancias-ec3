package com.canvia.constancias.util;

public class Constants {
	public static final String WS_SIGNER_PARAM_CLIENT_ID = "client_id";
	public static final String WS_SIGNER_PARAM_CLIENT_SECRET = "client_secret";
	public static final String WS_SIGNER_PARAM_CLIENT_GRANT_TYPE = "grant_type";
	public static final int WS_SIGNER_STATUS_OK = 200;
}
