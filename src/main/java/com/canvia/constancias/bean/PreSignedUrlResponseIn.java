package com.canvia.constancias.bean;

public class PreSignedUrlResponseIn {
	private String filename;
	private String processId;

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	
}