package com.canvia.constancias.bean;

import org.springframework.http.StreamingHttpOutputMessage.Body;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PreSignedUrlResponseOut {
	@JsonProperty("id")
	private String id;
	@JsonProperty("filename")
	private String filename;
	@JsonProperty("preSignedURL")
	private String preSignedURL;
	@JsonProperty("url")
	private String url;
	@JsonProperty("status")
	private String status;
	@JsonProperty("statusCore")
	private String statusCore;

	private int statusCode;
	private Body body;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getPreSignedURL() {
		return preSignedURL;
	}

	public void setPreSignedURL(String preSignedURL) {
		this.preSignedURL = preSignedURL;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusCore() {
		return statusCore;
	}

	public void setStatusCore(String statusCore) {
		this.statusCore = statusCore;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

}